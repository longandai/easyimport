package easyimport;

import java.util.Date;

import com.liuzhilong.util.easyimport.annotation.TargetColumn;
import com.liuzhilong.util.easyimport.annotation.TargetSheet;

@TargetSheet(name = "sheetName", mappingRow = 1, dataStartRow = 2)
public class User {
	@TargetColumn(name="name")
	private String username;
	
	@TargetColumn(name="password")
	private String password;
	
	@TargetColumn(name="created",dateFomat="YYYY-MM-DD hh:mm:ss")
	private Date created;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

}
