package easyimport;

import com.liuzhilong.util.easyimport.annotation.TargetColumn;
import com.liuzhilong.util.easyimport.annotation.TargetSheet;

import java.util.Date;

/**
 * Created by long on 2018/9/1.
 */
@TargetSheet(name = "useraccess", mappingRow = 2, dataStartRow = 3)
public class DpImportDTO {
    private String   permissionId;
    @TargetColumn(name="userCode")
    private String   userCode;
    @TargetColumn(name="fullName")
    private String   fullName;
    @TargetColumn(name="userCountry")
    private String   userCountryCode;
    @TargetColumn(name="dimType")
    private String   dimName;
    @TargetColumn(name="dimValue")
    private String   dimVal;
    @TargetColumn(name="dataStatus")
    private String   flag;
    private String   createUser;
    private Date createTime;
    private String   updateUser;
    private Date   updateTime;
    private String   remark;
    private String   dataStatus;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserCountryCode() {
        return userCountryCode;
    }

    public void setUserCountryCode(String userCountryCode) {
        this.userCountryCode = userCountryCode;
    }

    public String getDimName() {
        return dimName;
    }

    public void setDimName(String dimName) {
        this.dimName = dimName;
    }

    public String getDimVal() {
        return dimVal;
    }

    public void setDimVal(String dimVal) {
        this.dimVal = dimVal;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }
}
