package com.liuzhilong.util.easyimport.exception;
/**
 * @author liuzhilong
 * 没有对应的SHEET名称的时候爆出的异常
 */
public class NoSheetException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public NoSheetException() {
		super();
	}
	public NoSheetException(String message) {
		super(message);
	}
	private String sheetName;
	
	public String getSheetName() {
		return sheetName;
	}
	public NoSheetException setSheetName(String sheetName) {
		this.sheetName = sheetName;
		return this;
	}
	
}
