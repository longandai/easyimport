package com.liuzhilong.util.easyimport.exception;

/**
 * @author liuzhilong
 * longandai@163.com
 * 异常类 如果文件类型不属于 excel类型 提示此错误
 */

public class NotSupportFileTypeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NotSupportFileTypeException() {
		super();
	}
	public NotSupportFileTypeException(String message) {
		super(message);
	}
}
