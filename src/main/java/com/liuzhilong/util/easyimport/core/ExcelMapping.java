package com.liuzhilong.util.easyimport.core;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;



public interface ExcelMapping {
	
	/**
	 * 
	 * @return
	 */
	
	public List<?> list() throws InstantiationException, IllegalAccessException, InvocationTargetException, ParseException ;
	
}
