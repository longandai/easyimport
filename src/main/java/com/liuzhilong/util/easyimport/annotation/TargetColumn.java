package com.liuzhilong.util.easyimport.annotation;

import  java.lang.annotation.ElementType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * javabean 属性注解 
 * 主要是用于映射excel列转化属性
 * @author Administrator
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TargetColumn {
	
	/**
	 * 对应属性列表的名称 
	 * 如果是空值的话 默认取javabean自身
	 * @return
	 */
	public String name() default "";
	
	
	
	/**
	 * excel中的列的读取方式 按照什么格式读取
	 * 默认为STRING 类型
	 * @return
	 */
	public ExcelFieldType excelFieldType() default ExcelFieldType.STRING;
	
	
	
	
	
	/**
	 * 如果目标filed是Date类型，excel中按照 ，会地区此属性映射到date上
	 * @return
	 */
	public String dateFomat() default "YYYY-MM-DD hh24:mi:ss";
	
}
