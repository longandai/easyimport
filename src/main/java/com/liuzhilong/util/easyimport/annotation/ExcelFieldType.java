package com.liuzhilong.util.easyimport.annotation;
/**
 * @author liuzhilong
 * Excel列的单元格类型注解 
 *
 */
public enum ExcelFieldType {
	STRING,
	DATE,
	INT,
	FLOAT,
	BOOLEAN
}
