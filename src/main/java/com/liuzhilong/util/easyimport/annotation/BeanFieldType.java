package com.liuzhilong.util.easyimport.annotation;
	/**
	 * @author liuzhilong
	 * bean列的单元格类型注解
	 *
	 */
public enum BeanFieldType {
	
	STRING,
	DATE,
	INT,
	FLOAT,
	BOOLEAN

}
