package com.liuzhilong.util.easyimport.annotation;

import  java.lang.annotation.ElementType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author liuzhilong
 * longandai@163.com
 * 
 * 转化bean在 
 *
 */


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TargetSheet {
	
	
	
	/**
	 * 需要转换的sheet页面
	 * @return
	 */
	public String name();
	
	
	/**
	 * 转化所需的索引列 从0开始 
	 * 默认0
	 * @return
	 */
	public int mappingRow() default 0;
	

	/**
	 * 开始读取数据的列  
	 * 默认1
	 * @return
	 */
	public int dataStartRow() default 1;
	
	
}
