package com.liuzhilong.util.easyimport.util;

import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.liuzhilong.util.easyimport.annotation.TargetColumn;

public class ConverseUtil {
	public static Object converseData(String data,Field field) throws ParseException, InstantiationException, IllegalAccessException {
		Object result = data;
		if(field.getType().newInstance() instanceof Date && data !=null && !"".equals(data)) {
			String formatString = field.getAnnotation(TargetColumn.class).dateFomat();
			result = new SimpleDateFormat(formatString).parse(data);
		}
		return result;
	}
}
